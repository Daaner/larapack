<?php

namespace App\Repositories;

use Config;

abstract class InitRepository
{

  private $cache_enabled;
  protected $model;
  protected $cache_time;
  protected $locale;

  abstract protected function getModelClass();

  public function __construct() {
    $this->locale = Config::get('app.locale');
    //длительность кеша
    $this->cache_enabled = (bool) Config::get('app.cache_enabled');
    $this->cache_time = (int) Config::get('app.cache_time');

    $this->model = app($this->getModelClass());

    // Enable/disable cache repo
    if(!$this->cache_enabled) {
      $this->cache_time = 0;
    }
  }


  protected function startConditions() {
    return clone $this->model;
  }

}
