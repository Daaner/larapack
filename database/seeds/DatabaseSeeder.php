<?php

use Illuminate\Database\Seeder;

use Modules\User\Database\Seeders\UserDatabaseSeeder;
use Modules\BlogCategory\Database\Seeders\BlogCategoryDatabaseSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserDatabaseSeeder::class);
        $this->call(BlogCategoryDatabaseSeeder::class);
    }
}
