@extends('User::layouts.master')

@section('content')
  {{-- {{ \Auth::loginUsingId(1) }} --}}

  @if (auth()->user())
    <h1>Hello {{ auth()->user()->first_name }}</h1>

    {{-- {{ dd(auth()->user()->roles) }} --}}
      <p>
        <img src="{{ auth()->user()->AvatarOrBlank }}" alt="">
      </p>

    {{-- {{ Auth::logout() }}
    {{ Session::flush() }} --}}


  @else
    {{ \Auth::loginUsingId(2) }}
  @endif

  <p>
    This view is loaded from module: {!! config('user.name') !!}
  </p>
@endsection
