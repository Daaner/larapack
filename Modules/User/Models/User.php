<?php

namespace Modules\User\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\User\Traits\GravatarTrait;

class User extends Authenticatable
{
  use Notifiable, GravatarTrait;


  protected $table = 'users';


  protected $fillable = [
  ];


  protected $hidden = [
    'password',
    'remember_token',
  ];


  public function scopeActive($query) {
    return $query->where('active', true);
  }


  public function roles() {
    return $this->belongsToMany(Role::class, 'user_roles');
  }


  //admin password
  public function setNewPasswordAttribute($value) {
    if($value) {
      $this->attributes['password'] = bcrypt($value);
    }
  }

}
