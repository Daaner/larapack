<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;

use Modules\User\Models\User as Seedmodel;
use Hash;

class UsersTableSeeder extends Seeder
{

  public function run() {

    $datas = [
      [
        'first_name' => 'Андрей',
        'email' => 'daan@ukr.net',
        'password' => 'daan@ukr.net',
      ],
      [
        'first_name' => 'Кирилл',
        'email' => 'kastahoff@gmail.com',
        'password' => 'kastahoff@gmail.com',
      ],

    ];


    foreach ($datas as $data) {
      $newData = Seedmodel::where('email', $data['email'])->first();

      if ($newData === null) {
        $newData = Seedmodel::create(array(
          'first_name' => $data['first_name'],
          'email' => $data['email'],
          'password' => Hash::make($data['password']),
          'active' => true,
        ));
      }
    }

  }
}
