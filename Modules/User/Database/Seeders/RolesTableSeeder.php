<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;

use Modules\User\Models\Role as Seedmodel;

class RolesTableSeeder extends Seeder
{

  public function run() {

    $datas = [
      [
        'name_en' => 'Administrator',
        'name_ru' => 'Администратор',
        'name_ua' => 'Адміністратор',
      ],
      [
        'name_en' => 'Moderator',
        'name_ru' => 'Модератор',
        'name_ua' => 'Модератор',
      ],
      [
        'name_en' => 'User',
        'name_ru' => 'Пользователь',
        'name_ua' => 'Користувач',
      ],


    ];


    foreach ($datas as $data) {
      $newData = Seedmodel::where('name_en', $data['name_en'])->first();

      if ($newData === null) {
        $newData = Seedmodel::create(array(
          'name_en' => $data['name_en'],
          'name_ru' => $data['name_ru'],
          'name_ua' => $data['name_ua'],
        ));
      }
    }

  }
}
