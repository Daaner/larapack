
{{-- BlogCategory --}}
@if (isset($categories))
  @include('BlogCategory::children', [
    'cat' => $categories
  ])
@else
  Нет данных с категории
@endif
