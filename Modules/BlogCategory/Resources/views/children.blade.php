<ul class="navbar-nav mr-auto">
  @foreach ($cat as $key => $children)
    @if($children->childrens)
      <li class="nav-item dropdown">
        <a href="{{ $children->slug }}" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"">
          {{ $children->title }}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          @foreach($children->childrens as $key => $children)
            <a class="dropdown-item" href="{{ $children->slug }}">{{ $children->title }}</a>
          @endforeach
        </div>
        {{-- @if ($children->childrens)
          <div class="dropdown-divider"></div>
          @include('BlogCategory::children', [
            'cat' => $children->childrens
          ])
        @endif --}}
      </li>
    @else
      <li class="nav-item">
        <a class="nav-link" href="#">{{ $children->title }}</a>
      </li>
    @endif
  @endforeach
</ul>
