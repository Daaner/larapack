<?php

namespace Modules\BlogCategory\Http\ViewComposers;

use Illuminate\View\View;

use Modules\BlogCategory\Repositories\BlogCategoryRepository;

class CategoryComposer
{

  private $categories;


  public function __construct() {

    $this->categories = collect();

    $repo = new BlogCategoryRepository;
    $this->categories = $repo->getBlogCategories();
    // $this->categories = $repo->getCacheBlogCategories();

  }


  public function compose(View $view) {
    $view->with([
      'categories' => $this->categories,
    ]);
  }

}
