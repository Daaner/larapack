<?php

namespace Modules\BlogCategory\Http\Controllers;

// use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\BlogCategory\Models\Category;

use Modules\BlogCategory\Repositories\BlogCategoryRepository;


class BlogCategoryController extends Controller
{

  public function index() {

    // cache()->flush();

    $repo = new BlogCategoryRepository;

    // $categories = $repo->getBlogCategories();
    $categories = $repo->getCacheBlogCategories();

    // dd($categories);


    // ddd($categories);
    // dd($categories);

    return view('BlogCategory::index', [
      'categories' => $categories,
    ]);
  }

}
