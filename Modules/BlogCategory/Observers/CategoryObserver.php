<?php

namespace Modules\BlogCategory\Observers;

use Modules\BlogCategory\Models\Category as Model;

class CategoryObserver
{

  public function __construct() {
  }


  public function Clear() {
    $locales = config()->get('app.locales');

    foreach ($locales as $key => $locale) {
      $cache_name = 'blog_categories_' . $locale;
      cache()->forget($cache_name);
    }

    // $cache_name_tag = 'blog_categories';
    // // Cache::tags('authors')->flush();
    // cache()->tags($cache_name)->flush();
  }


  // public function creating(Model $result) {
  // }
  public function created(Model $result) {
    $this->Clear();
  }

  // public function updating(Model $result) {
  //   $result->active = false;
  // }
  public function updated(Model $result) {
    $this->Clear();
  }

  // public function deleting(Model $result) {
  //   if($result->isForceDeleting()) {
  //     dd('удаляем навсегда');
  //   }
  // }
  public function deleted(Model $result) {
    $this->Clear();
  }

  // public function restoring(Model $result) {
  // }
  public function restored(Model $result) {
    $this->Clear();
  }

}
