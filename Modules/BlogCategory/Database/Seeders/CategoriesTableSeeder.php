<?php

namespace Modules\BlogCategory\Database\Seeders;

use Illuminate\Database\Seeder;

use Modules\BlogCategory\Models\Category as Seedmodel;

class CategoriesTableSeeder extends Seeder
{

  public function run() {

    $datas = [
      [
        'title_en' => 'News',
        'title_ru' => 'Новости',
        'title_ua' => 'Новини',
        'slug' => 'news',
        'parent_id' => null,
      ],
      [
        'title_en' => 'Ukraine',
        'title_ru' => 'Украина',
        'title_ua' => 'Україна',
        'slug' => 'ukraine',
        'parent_id' => 1,
      ],
      [
        'title_en' => 'Russia',
        'title_ru' => 'Россия',
        'title_ua' => 'Росія',
        'slug' => 'russia',
        'parent_id' => 1,
      ],
      [
        'title_en' => 'Krivoy Rog',
        'title_ru' => 'Кривой Рог',
        'title_ua' => 'Кривий Ріг',
        'slug' => 'ua-krivoy-rog',
        'parent_id' => 2,
      ],
      [
        'title_en' => 'Kiev',
        'title_ru' => 'Киев',
        'title_ua' => 'Київ',
        'slug' => 'kiev',
        'parent_id' => 2,
      ],
    ];


    foreach ($datas as $data) {
      $newData = Seedmodel::where('slug', $data['slug'])->first();

      if ($newData === null) {
        $newData = Seedmodel::create(array(
          'title_en' => $data['title_en'],
          'title_ru' => $data['title_ru'],
          'title_ua' => $data['title_ua'],
          'slug' => $data['slug'],
          'active' => true,
          'parent_id' => $data['parent_id'],
        ));
      }
    }

  }
}
