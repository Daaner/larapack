<?php

namespace Modules\BlogCategory\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\BlogCategory\Database\Seeders\CategoriesTableSeeder;
use Modules\BlogCategory\Database\Seeders\CategoriesFakeTableSeeder;

class BlogCategoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(CategoriesTableSeeder::class);
        // $this->call(CategoriesFakeTableSeeder::class);
    }
}
