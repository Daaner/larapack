<?php

namespace Modules\BlogCategory\Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Str;

use Modules\BlogCategory\Models\Category as Seedmodel;

class CategoriesFakeTableSeeder extends Seeder
{

  public function run() {


    $faker = Faker::create('ru_RU');

    for ($i=0; $i < 50; $i++) {
      $name = $faker->company;

      $newData = Seedmodel::where('slug', Str::slug($name))->first();

      if ($newData === null) {
        $newData = Seedmodel::create(array(
          'title_en' => $name,
          'title_ru' => $name,
          'title_ua' => $name,
          'slug' => Str::slug($name),
          'active' => true,
          'parent_id' => rand(0, 4),
        ));
      };

    };


  }
}
