<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BlogCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('blog_categories', function (Blueprint $table) {
          $table->bigIncrements('id');

          $table->string('title_en');
          $table->string('title_ru');
          $table->string('title_ua');

          $table->string('slug')->unique();
          $table->boolean('active')->default(false);
          $table->bigInteger('parent_id')->nullable();
          $table->integer('order')->default(0);

          $table->softDeletes();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('users');
    }
}
