<?php

namespace Modules\BlogCategory\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Contracts\View\Factory as ViewFactory;
use View;

use Modules\BlogCategory\Models\Category;
use Modules\BlogCategory\Observers\CategoryObserver;

use Modules\BlogCategory\Http\ViewComposers\CategoryComposer;


class BlogCategoryServiceProvider extends ServiceProvider
{

  private $views;

  private function compose($views, string $viewComposer){
    $this->app->singleton($viewComposer);
    $this->views->composer($views, $viewComposer);
  }

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot(ViewFactory $viewFactory)
    {

      $this->views = $viewFactory;
      $this->compose('BlogCategory::index', CategoryComposer::class);

        Category::observe(CategoryObserver::class);

        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path('BlogCategory', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('BlogCategory', 'Config/config.php') => config_path('blogcategory.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('BlogCategory', 'Config/config.php'), 'blogcategory'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/blogcategory');

        $sourcePath = module_path('BlogCategory', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/blogcategory';
        }, \Config::get('view.paths')), [$sourcePath]), 'BlogCategory');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/blogcategory');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'blogcategory');
        } else {
            $this->loadTranslationsFrom(module_path('BlogCategory', 'Resources/lang'), 'blogcategory');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

}
