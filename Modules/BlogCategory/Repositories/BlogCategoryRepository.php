<?php

namespace Modules\BlogCategory\Repositories;

use Modules\BlogCategory\Models\Category as Model;
use App\Repositories\InitRepository;

use Cache;
use Config;

class BlogCategoryRepository extends InitRepository
{

  public function __construct() {
    parent::__construct();
  }

  protected function getModelClass() {
    return Model::class;
  }


  public function getBlogCategories() {

    $columns = [
      'id',
      'title_'. $this->locale .' as title',
      'slug',
      'parent_id',
      'order',
      'active',
    ];

    $result = $this->startConditions()
      ->active() //scope
      ->where('parent_id', null)
      // ->orWhere('parent_id', 0)
      ->select($columns)
      ->with('childrens:id,title_'. $this->locale .' as title,slug,parent_id')
      ->orderBy('order', 'asc')
      ->get();

    return $result;
  }


  public function getCacheBlogCategories() {
    $cache_name = 'blog_categories_' . $this->locale;

    if (Cache::has($cache_name) && !Cache::get($cache_name)->isEmpty()) {
      $data_cache = Cache::get($cache_name);
    } else {
      $data_cache = Cache::remember($cache_name, $this->cache_time, function () {
        return $this->getBlogCategories();
      });
    }

    return $data_cache;
  }

}
