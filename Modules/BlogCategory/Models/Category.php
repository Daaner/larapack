<?php

namespace Modules\BlogCategory\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class Category extends Model
{

  protected $table = 'blog_categories';

  protected $fillable = [];


  public function scopeActive($query){
    return $query->where('active', true);
  }


  public function parent() {
    return $this
      // ->active()
      ->belongsTo(Category::class, 'parent_id')
      ->withDefault([
        'title' => '-',
        'title_ru' => '-',
        'title_en' => '-',
        'title_ua' => '-',
      ]);
  }

  public function childrens() {
    $locale = Config::get('app.locale');

    return $this
      ->hasMany(Category::class, 'parent_id', 'id')
      ->with('childrens:id,title_'. $locale .' as title,slug,parent_id');
  }




}
