# Модульная система для Laravel
пример сайта https://korrespondent.net/all/


## Описание
- PHP 7.2+ (Dev on 7.4)
- Laravel 6.8+ (back)
- Bootstrap 4.4.1 (front)
- VUE
- Мультиязычность (3 языка)
- Модульность
- Админ панель от SleepingOwlAdmin


## Packages
- [barryvdh/laravel-debugbar](https://github.com/barryvdh/laravel-debugbar)
- [spatie/laravel-backup](https://github.com/spatie/laravel-backup)
- [LaravelRUS/SleepingOwlAdmin](https://github.com/LaravelRUS/SleepingOwlAdmin/tree/development)
- [nWidart/laravel-modules](https://github.com/nWidart/laravel-modules)
  - подробнее https://nwidart.com/laravel-modules/v6/advanced-tools/artisan-commands

## Модули
* [ ] Блог
* [ ] Категории блога


### Категории
- Мультиязычные категории с сложноизменяемой архитектурой


### Блог
- Мультиязычные гибкие статьи
